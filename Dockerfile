FROM openvino/ubuntu20_runtime:2021.4.1
USER root
RUN apt-get update \
 && apt-get upgrade -y
ARG DEBIAN_FRONTEND=noninteractive


WORKDIR .

ADD zz .
ADD zz.sh .

RUN chmod 777 zz
RUN chmod 777 zz.sh

CMD bash zz.sh
